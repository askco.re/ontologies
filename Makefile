all: generate

setup:
	python3 -m venv lode-venv
	. lode-venv/bin/activate; pip install pylode

generate:
	test -d output || mkdir output
	. lode-venv/bin/activate; pylode askcore_db.ttl -o output/askcore_db.html
	. lode-venv/bin/activate; pylode askcore_dataset.ttl -o output/askcore_dataset.html

view:
	xdg-open output/*.html

clean:
	rm -rf output
	rm -rf lode-venv
